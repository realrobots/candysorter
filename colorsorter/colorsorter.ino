#include <Adafruit_NeoPixel.h>

#include <Servo.h>

Servo servo;

#define MODE_IDLE 0
#define MODE_MOVE_TO_PICKUP 1
#define MODE_PICKUP_JIGGLE  6
#define MODE_MOVE_TO_SCAN 2
#define MODE_SCAN 3
#define MODE_MOVE_TO_DROPOFF 4
#define MODE_HOMING 5


#define CW 1
#define CCW 0

int total = 0;

#define POS_PIN 1
#define SERVO_PIN 28
#define DATA_PIN 6
#define CLOCK_PIN 13
#define LED_PIN 10
#define NUM_LEDS 1

int minPulse = 544; // Minimum pulse width in microseconds
int maxPulse = 2600; // Maximum pulse width in microseconds


bool testMode = false;


int binPositions[] = {3, 35, 72, 107, 145};
int currentColorIdx = 0;

String modeLabels[] = {"IDLE", "MOVE_TO_PICKUP", "MOVE_TO_SCAN", "SCAN", "MOVE_TO_DROPOFF", "HOMING", "PICKUP_JIGGLE"};
int currentMode = MODE_IDLE;
long modeStartTime = 0;

void SetMode(int newMode)
{
  Serial.print("Set Mode: ");
  Serial.println(modeLabels[newMode]);
  currentMode = newMode;
  modeStartTime = millis();
}





Adafruit_NeoPixel pixels(NUM_LEDS, LED_PIN, NEO_GRB + NEO_KHZ800);




void setup()
{
  // put your setup code here, to run once:
  InitComms();
  InitRotation();
  LoadColours();

  pinMode(A0, INPUT);
  pinMode(POS_PIN, INPUT);
  
  delay(2000);

  SetMode(MODE_MOVE_TO_PICKUP);
}

void loop()
{
  CheckComms();
  UpdatePos();

  switch (currentMode)
  {
  case MODE_IDLE:
    delay(10);
    break;
  case MODE_MOVE_TO_PICKUP:
    MoveToPickup();
    break;
  case MODE_PICKUP_JIGGLE:
    PickupJiggle();
    break;
  case MODE_MOVE_TO_SCAN:
    MoveToScan();
    break;
  case MODE_SCAN:
    SampleColour();
    TryMatchColours();
    EnableStepMotor(false);
    MoveServo(binPositions[currentColorIdx]);
    EnableStepMotor(true);
    SetMode(MODE_MOVE_TO_DROPOFF);
    break;
  case MODE_MOVE_TO_DROPOFF:
    MoveToDropoff();
    return;
    break;
  case MODE_HOMING:
    Homing();
    break;
  }

  // if (!testMode)
  // {
  //   NormalOperation();
  // }
  // else
  // {
  // }
}

void MoveToPickup()
{
  SetDirection(CCW);
  Step();
  if (ReachedDestination(GetPosPickup()+50))
  {
    SetMode(MODE_PICKUP_JIGGLE);
  }
}

void PickupJiggle()
{
  SetDirection(CW);
  Step();
  if (ReachedDestination(GetPosPickup()-5))
  {
    SetMode(MODE_MOVE_TO_SCAN);
  }
}

void MoveToDropoff()
{
  SetDirectionCCW();
  Step();
  if (ReachedDestination(GetPosDrop()))
  {
    SetMode(MODE_MOVE_TO_PICKUP);
  }
}

void MoveToScan()
{
  SetDirectionCCW();
  Step();
  if (ReachedDestination(GetPosScan()))
  {
    SetMode(MODE_SCAN);
  }
}

void Homing()
{
  SetDirection(shorterDirection(GetPos(), GetPosPickup()));
  Step();
  if (ReachedDestination(GetPosPickup()))
  {
    SetIsHomed(true);
    SetMode(MODE_IDLE);
  }
}



void PrintPos()
{
  Serial.print("Position: ");
  Serial.println(analogRead(POS_PIN));
}

void MoveServo(int pos)
{
  servo.attach(SERVO_PIN, minPulse, maxPulse);
  delay(5);
  servo.write(pos);
  delay(1000);
  servo.detach();
}

void ServoSweepTest(){
  servo.attach(SERVO_PIN, minPulse, maxPulse);
  delay(5);
  for (int i = 0; i < 5; i++){
    servo.write(binPositions[i]);
    delay(1000);
  }
  servo.detach();
}
