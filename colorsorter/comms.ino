#define COMMAND_SIZE 256
#define VERSION "1.0.0"

static char data[COMMAND_SIZE];
static int serial_count;
static char c;

void InitComms()
{
    Serial.begin(115200);
    ReportID();

    delay(400);
}

void ReportID(){
    Serial.print("Realrobots Candy Sorter v");
    Serial.println(VERSION);
}

void CheckComms()
{
    while (Serial.available() > 0)
    {
        c = Serial.read();
        //Serial.print(c);
        if (serial_count < COMMAND_SIZE)
        {
            data[serial_count] = c;
            serial_count++;
        }
    }

    // Incoming packet should be at least one byte plus the EOL \r\n
    if (serial_count > 0)
    {
        
        if (data[serial_count - 2] == '\r' && data[serial_count - 1] == '\n')
        {
            InterpretData();
        }
        ClearData();
    }
    else
    {
        ClearData();
    }
}

void InterpretData()
{
    Serial.println("interpreting");

    if (data[0] == '\r' || data[0] == '\n')
    {
        return;
    }

    if (data[0] == '?')
    {
        ReportID();

        Serial.print("\n");
        Serial.print("Normal operations suspended, unit in test mode.\n");
        Serial.print("h\tHome Test\n");
        Serial.print("d\tDisable Lazy Susan Motor\n");
        Serial.print("a\tEnable Lazy Suzan Motor\n");
        Serial.print("s\tScan Colour Test\n");
        Serial.print("v\tServo Sweep Test\n");
        Serial.print("1\tSet Pickup Position\n");
        Serial.print("2\tSet Scan Position\n");
        Serial.print("3\tSet Dropoff Position\n");
        Serial.print("5\tCalibrate YELLOW\n");
        Serial.print("6\tCalibrate GREEN\n");
        Serial.print("7\tCalibrate ORANGE\n");
        Serial.print("8\tCalibrate BROWN\n");
        Serial.print("9\tCalibrate RED\n");
        Serial.print("0\tCalibrate OPEN\n");
        Serial.print("-\tCalibrate CLOSED\n");        
        Serial.print("p\tPrint EEPROM\n");
        Serial.print("n\tNormal mode\n");
        Serial.print("i\tIdle mode\n");
        Serial.println();

        SetMode(MODE_IDLE);
    }
    else if (data[0] == 'p')
    {
        Serial.println("Printing EEPROM");
        EEPROMReport();
    }
    else if (data[0] == 'h')
    {
        Serial.println("Homing");
        SetMode(MODE_HOMING);        
    }
    
    else if (data[0] == 's')
    {
        Serial.println("Testing Colour sense");
        SampleColour();
        TryMatchColours();  
    }
    else if (data[0] == 'v')
    {
        Serial.println("Testing Servo");
        ServoSweepTest();
    }
    else if (data[0] == 'd')
    {
        Serial.println("Disable Lazy Susan Motor");
        EnableStepMotor(false);     
    }
    else if (data[0] == 'a')
    {
        Serial.println("Enable Lazy Suzan Motor");
        EnableStepMotor(true);       
    }
    else if (data[0] == 'n')
    {
        
        EnableStepMotor(true);
        SetMode(MODE_MOVE_TO_PICKUP);       
    }
    else if (data[0] == 'i')
    {
        
        SetMode(MODE_IDLE);       
    }
    else if (data[0] == '1')
    {
        
        SavePosPickup();     
    }
    else if (data[0] == '2')
    {
        
        SavePosScan();     
    }
    else if (data[0] == '3')
    {
        
        SavePosDrop();     
    }
    else if (data[0] == '5')
    {
        
        SampleColour();
        StoreCurrentColour(0);    
    }
    else if (data[0] == '6')
    {
        
        SampleColour();
        StoreCurrentColour(1);    
    }
    else if (data[0] == '7')
    {
        
        SampleColour();
        StoreCurrentColour(2);    
    }
    else if (data[0] == '8')
    {
        
        SampleColour();
        StoreCurrentColour(3);    
    }
    else if (data[0] == '9')
    {
        
        SampleColour();
        StoreCurrentColour(4);    
    }
    else if (data[0] == '0')
    {
        
        SampleColour();
        StoreCurrentColour(5);    
    }
    else if (data[0] == '-')
    {
        
        SampleColour();
        StoreCurrentColour(6);    
    }
}

void ClearData()
{
    for (int i = 0; i < COMMAND_SIZE; i++)
    {
        data[i] = 0;
    }
    serial_count = 0;
}
