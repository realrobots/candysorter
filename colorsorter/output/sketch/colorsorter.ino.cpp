#include <Arduino.h>
#line 1 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
#include <FastLED.h>
#include <Servo.h>

Servo servo;

#define MODE_IDLE 0
#define MODE_MOVE_TO_PICKUP 1
#define MODE_PICKUP_JIGGLE  6
#define MODE_MOVE_TO_SCAN 2
#define MODE_SCAN 3
#define MODE_MOVE_TO_DROPOFF 4
#define MODE_HOMING 5


#define CW 1
#define CCW 0

String modeLabels[] = {"IDLE", "MOVE_TO_PICKUP", "MOVE_TO_SCAN", "SCAN", "MOVE_TO_DROPOFF", "HOMING", "PICKUP_JIGGLE"};
int currentMode = MODE_IDLE;
long modeStartTime = 0;

#line 22 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void SetMode(int newMode);
#line 54 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void setup();
#line 69 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void loop();
#line 114 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void MoveToPickup();
#line 124 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void PickupJiggle();
#line 134 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void MoveToDropoff();
#line 144 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void MoveToScan();
#line 154 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void Homing();
#line 167 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void PrintPos();
#line 173 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void MoveServo(int pos);
#line 182 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void ServoSweepTest();
#line 20 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"
void LoadColours();
#line 25 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"
void SetColour(int colorIdx, int r, int g, int b);
#line 32 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"
void RollingAverage();
#line 48 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"
void SampleColour();
#line 76 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"
void StoreCurrentColour(int colorIdx);
#line 91 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"
void TryMatchColours();
#line 139 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"
int GetAverage();
#line 151 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"
int GetRollingAverage(int colorIdx);
#line 179 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"
int DistanceFromAverage();
#line 8 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\comms.ino"
void InitComms();
#line 16 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\comms.ino"
void ReportID();
#line 21 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\comms.ino"
void CheckComms();
#line 50 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\comms.ino"
void InterpretData();
#line 189 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\comms.ino"
void ClearData();
#line 11 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
void EEPROMReport();
#line 29 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
bool CheckIsFirstRun();
#line 54 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
void LoadColoursFromEEPROM();
#line 64 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
void StoreColour(uint8_t colorIdx, uint16_t r, uint16_t g, uint16_t b);
#line 71 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
void StorePickupPos(uint16_t pos);
#line 76 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
int LoadPickupPos();
#line 81 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
void StoreScanPos(uint16_t pos);
#line 86 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
int LoadScanPos();
#line 91 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
void StoreDropoffPos(uint16_t pos);
#line 96 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
int LoadDropoffPos();
#line 101 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
void Store8BitValue(uint16_t idx, uint8_t val);
#line 106 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
void Store16BitValue(uint16_t idx, int val);
#line 112 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
uint8_t Read8BitValue(uint16_t idx);
#line 117 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
int Read16BitValue(uint16_t idx);
#line 22 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void InitRotation();
#line 46 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
bool IsHomed();
#line 50 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void SetIsHomed(bool _isHomed);
#line 54 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void Vibrate();
#line 72 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
int shorterDirection(int crt, int next);
#line 86 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
bool ReachedDestination(int targetPos);
#line 93 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void SetDirection(int dir);
#line 98 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void SetDirectionCW();
#line 103 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void SetDirectionCCW();
#line 108 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void UpdatePos();
#line 112 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void Step();
#line 120 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void Step(int steps, int dir);
#line 130 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void EnableStepMotor(bool enabled);
#line 144 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
int Difference(int v0, int v1);
#line 156 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
int GetPos();
#line 160 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
int GetPosPickup();
#line 164 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
int GetPosDrop();
#line 168 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
int GetPosScan();
#line 172 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void SavePosPickup();
#line 179 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void SavePosDrop();
#line 186 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"
void SavePosScan();
#line 22 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorsorter.ino"
void SetMode(int newMode)
{
  Serial.print("Set Mode: ");
  Serial.println(modeLabels[newMode]);
  currentMode = newMode;
  modeStartTime = millis();
}

// How many leds in your strip?
#define NUM_LEDS 1



CRGB leds[NUM_LEDS];
int total = 0;

#define POS_PIN A1
#define SERVO_PIN 7
#define DATA_PIN 6
#define CLOCK_PIN 13




bool testMode = false;


int binPositions[] = {3, 35, 72, 107, 145};
int currentColorIdx = 0;



void setup()
{
  // put your setup code here, to run once:
  InitComms();
  InitRotation();
  LoadColours();

  pinMode(A0, INPUT);
  pinMode(POS_PIN, INPUT);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
  delay(2000);

  SetMode(MODE_MOVE_TO_PICKUP);
}

void loop()
{
  CheckComms();
  UpdatePos();

  switch (currentMode)
  {
  case MODE_IDLE:
    delay(10);
    break;
  case MODE_MOVE_TO_PICKUP:
    MoveToPickup();
    break;
  case MODE_PICKUP_JIGGLE:
    PickupJiggle();
    break;
  case MODE_MOVE_TO_SCAN:
    MoveToScan();
    break;
  case MODE_SCAN:
    SampleColour();
    TryMatchColours();
    EnableStepMotor(false);
    MoveServo(binPositions[currentColorIdx]);
    EnableStepMotor(true);
    SetMode(MODE_MOVE_TO_DROPOFF);
    break;
  case MODE_MOVE_TO_DROPOFF:
    MoveToDropoff();
    return;
    break;
  case MODE_HOMING:
    Homing();
    break;
  }

  // if (!testMode)
  // {
  //   NormalOperation();
  // }
  // else
  // {
  // }
}

void MoveToPickup()
{
  SetDirection(CCW);
  Step();
  if (ReachedDestination(GetPosPickup()+50))
  {
    SetMode(MODE_PICKUP_JIGGLE);
  }
}

void PickupJiggle()
{
  SetDirection(CW);
  Step();
  if (ReachedDestination(GetPosPickup()-5))
  {
    SetMode(MODE_MOVE_TO_SCAN);
  }
}

void MoveToDropoff()
{
  SetDirectionCCW();
  Step();
  if (ReachedDestination(GetPosDrop()))
  {
    SetMode(MODE_MOVE_TO_PICKUP);
  }
}

void MoveToScan()
{
  SetDirectionCCW();
  Step();
  if (ReachedDestination(GetPosScan()))
  {
    SetMode(MODE_SCAN);
  }
}

void Homing()
{
  SetDirection(shorterDirection(GetPos(), GetPosPickup()));
  Step();
  if (ReachedDestination(GetPosPickup()))
  {
    SetIsHomed(true);
    SetMode(MODE_IDLE);
  }
}



void PrintPos()
{
  Serial.print("Position: ");
  Serial.println(analogRead(POS_PIN));
}

void MoveServo(int pos)
{
  servo.attach(SERVO_PIN);
  delay(5);
  servo.write(pos);
  delay(1000);
  servo.detach();
}

void ServoSweepTest(){
  servo.attach(SERVO_PIN);
  delay(5);
  for (int i = 0; i < 5; i++){
    servo.write(binPositions[i]);
    delay(1000);
  }
  servo.detach();
}

#line 1 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\colorscan.ino"

int averageRed[32];
int averageGreen[32];
int averageBlue[32];
int count = 0;

int color[] = {0, 0, 0};
bool averageReady = false;

int colors[7][3] = {{972, 941, 940},  // YELLOW
                    {955, 933, 937},  // GREEN
                    {973, 927, 936},  // ORANGE
                    {956, 921, 935},  // BROWN
                    {965, 920, 934},  // RED
                    {800, 800, 800},  // OPEN
                    {972, 944, 956}}; // WHITE (CLOSED)

String colorLabels[] = {"YELLOW", "GREEN", "ORANGE", "BROWN", "RED", "OPEN", "CLOSED"};

void LoadColours()
{
  LoadColoursFromEEPROM();
}

void SetColour(int colorIdx, int r, int g, int b)
{
  colors[colorIdx][0] = r;
  colors[colorIdx][1] = g;
  colors[colorIdx][2] = b;
}

void RollingAverage()
{
  if (count < 32)
  {
    averageRed[count] = color[0];
    averageGreen[count] = color[1];
    averageBlue[count] = color[2];
    count++;
  }
  else
  {
    averageReady = true;
    count = 0;
  }
}

void SampleColour()
{

  leds[0] = CRGB::Red;
  FastLED.show();
  color[0] = GetAverage();

  leds[0] = CRGB::Green;
  FastLED.show();
  delay(5);
  color[1] = GetAverage();

  leds[0] = CRGB::Blue;
  FastLED.show();
  color[2] = GetAverage();

  leds[0] = CRGB::Black;
  FastLED.show();

  Serial.print("R: ");
  Serial.print(color[0]);
  Serial.print("\tG: ");
  Serial.print(color[1]);
  Serial.print("\tB: ");
  Serial.print(color[2]);
  Serial.println();
}

void StoreCurrentColour(int colorIdx)
{
  Serial.print("Storing ");
  Serial.print(colorLabels[colorIdx]);
  Serial.print(": ");
  Serial.print(color[0]);
  Serial.print("\t");
  Serial.print(color[1]);
  Serial.print("\t");
  Serial.print(color[2]);
  Serial.print("\n");
  StoreColour(colorIdx, color[0], color[1], color[2]);
  SetColour(colorIdx, color[0], color[1], color[2]);
}

void TryMatchColours()
{
  int match[] = {0, 0, 0, 0, 0};
  int r;
  int g;
  int b;
  int closest = 0;
  // Serial.print('R: ');
  // Serial.print(color[0]);
  // Serial.print('\tG: ');
  // Serial.print(color[1]);
  // Serial.print('\tB: ');
  // Serial.print(color[2]);
  // Serial.println('\n');

  //Serial.print("Match\t");
  for (int i = 0; i < 5; i++)
  {
    r = Difference(color[0], colors[i][0]);
    g = Difference(color[1], colors[i][1]);
    b = Difference(color[2], colors[i][2]);
    match[i] = r + g + b;

    // Serial.print(colorLabels[i]);
    // Serial.print(":");
    // Serial.print(match[i]);
    // Serial.print("\t");
    if (i != 0)
    {
      if (match[i] < match[closest])
      {
        closest = i;
      }
    }
  }
  Serial.println();
  Serial.print("I guess the colour is ");
  Serial.println(colorLabels[closest]);
  currentColorIdx = closest;

  Serial.print(colors[closest][0]);
  Serial.print("\t");
  Serial.print(colors[closest][1]);
  Serial.print("\t");
  Serial.print(colors[closest][2]);
  Serial.print("\n");
}

int GetAverage()
{
  total = 0;
  delay(3);
  for (int i = 0; i < 32; i++)
  {
    total += analogRead(A0);
    delay(1);
  }
  return total / 32;
}

int GetRollingAverage(int colorIdx)
{
  if (!averageReady)
  {
    return -100;
  }
  else
  {
    int total = 0;
    for (int i = 0; i < 32; i++)
    {
      if (colorIdx == 0)
      {
        total += averageRed[i];
      }
      else if (colorIdx == 1)
      {
        total += averageGreen[i];
      }
      else
      {
        total += averageBlue[i];
      }
    }
    return total / 32;
  }
}

int DistanceFromAverage()
{
  int colorNow = (color[0] + color[1] + color[2]) / 3;
  int average = (GetRollingAverage(0) + GetRollingAverage(1) + GetRollingAverage(2)) / 3;

  return abs(colorNow - average);
}

#line 1 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\comms.ino"
#define COMMAND_SIZE 256
#define VERSION "1.0.0"

static char data[COMMAND_SIZE];
static int serial_count;
static char c;

void InitComms()
{
    Serial.begin(115200);
    ReportID();

    delay(400);
}

void ReportID(){
    Serial.print("Realrobots Candy Sorter v");
    Serial.println(VERSION);
}

void CheckComms()
{
    while (Serial.available() > 0)
    {
        c = Serial.read();
        //Serial.print(c);
        if (serial_count < COMMAND_SIZE)
        {
            data[serial_count] = c;
            serial_count++;
        }
    }

    // Incoming packet should be at least one byte plus the EOL \r\n
    if (serial_count > 0)
    {
        
        if (data[serial_count - 2] == '\r' && data[serial_count - 1] == '\n')
        {
            InterpretData();
        }
        ClearData();
    }
    else
    {
        ClearData();
    }
}

void InterpretData()
{
    Serial.println("interpreting");

    if (data[0] == '\r' || data[0] == '\n')
    {
        return;
    }

    if (data[0] == '?')
    {
        ReportID();

        Serial.print("\n");
        Serial.print("Normal operations suspended, unit in test mode.\n");
        Serial.print("h\tHome Test\n");
        Serial.print("d\tDisable Lazy Susan Motor\n");
        Serial.print("a\tEnable Lazy Suzan Motor\n");
        Serial.print("s\tScan Colour Test\n");
        Serial.print("v\tServo Sweep Test\n");
        Serial.print("1\tSet Pickup Position\n");
        Serial.print("2\tSet Scan Position\n");
        Serial.print("3\tSet Dropoff Position\n");
        Serial.print("5\tCalibrate YELLOW\n");
        Serial.print("6\tCalibrate GREEN\n");
        Serial.print("7\tCalibrate ORANGE\n");
        Serial.print("8\tCalibrate BROWN\n");
        Serial.print("9\tCalibrate RED\n");
        Serial.print("0\tCalibrate OPEN\n");
        Serial.print("-\tCalibrate CLOSED\n");        
        Serial.print("p\tPrint EEPROM\n");
        Serial.print("n\tNormal mode\n");
        Serial.print("i\tIdle mode\n");
        Serial.println();

        SetMode(MODE_IDLE);
    }
    else if (data[0] == 'p')
    {
        Serial.println("Printing EEPROM");
        EEPROMReport();
    }
    else if (data[0] == 'h')
    {
        Serial.println("Homing");
        SetMode(MODE_HOMING);        
    }
    
    else if (data[0] == 's')
    {
        Serial.println("Testing Colour sense");
        SampleColour();
        TryMatchColours();  
    }
    else if (data[0] == 'v')
    {
        Serial.println("Testing Servo");
        ServoSweepTest();
    }
    else if (data[0] == 'd')
    {
        Serial.println("Disable Lazy Susan Motor");
        EnableStepMotor(false);     
    }
    else if (data[0] == 'a')
    {
        Serial.println("Enable Lazy Suzan Motor");
        EnableStepMotor(true);       
    }
    else if (data[0] == 'n')
    {
        
        EnableStepMotor(true);
        SetMode(MODE_MOVE_TO_PICKUP);       
    }
    else if (data[0] == 'i')
    {
        
        SetMode(MODE_IDLE);       
    }
    else if (data[0] == '1')
    {
        
        SavePosPickup();     
    }
    else if (data[0] == '2')
    {
        
        SavePosScan();     
    }
    else if (data[0] == '3')
    {
        
        SavePosDrop();     
    }
    else if (data[0] == '5')
    {
        
        SampleColour();
        StoreCurrentColour(0);    
    }
    else if (data[0] == '6')
    {
        
        SampleColour();
        StoreCurrentColour(1);    
    }
    else if (data[0] == '7')
    {
        
        SampleColour();
        StoreCurrentColour(2);    
    }
    else if (data[0] == '8')
    {
        
        SampleColour();
        StoreCurrentColour(3);    
    }
    else if (data[0] == '9')
    {
        
        SampleColour();
        StoreCurrentColour(4);    
    }
    else if (data[0] == '0')
    {
        
        SampleColour();
        StoreCurrentColour(5);    
    }
    else if (data[0] == '-')
    {
        
        SampleColour();
        StoreCurrentColour(6);    
    }
}

void ClearData()
{
    for (int i = 0; i < COMMAND_SIZE; i++)
    {
        data[i] = 0;
    }
    serial_count = 0;
}

#line 1 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\preferences.ino"
#include <EEPROM.h>

int defaultColors[7][3] = {{972, 941, 940},  // YELLOW
                        {955, 933, 937},  // GREEN
                        {973, 927, 936},  // ORANGE
                        {956, 921, 935},  // BROWN
                        {965, 920, 934},  // RED
                        {800, 800, 800},   // OPEN
                        {972, 944, 956}}; // WHITE (CLOSED)

void EEPROMReport(){
    
    Serial.print("PICKUP POS: ");
    Serial.println(LoadPickupPos());
    Serial.print("DROP POS: ");
    Serial.println(LoadDropoffPos());
    Serial.print("SCAN POS: ");
    Serial.println(LoadScanPos());


    Serial.println("COLORS");
    for (int i = 10; i < 10+8*8 + 4; i+= 2){
        Serial.print(i);
        Serial.print(": ");
        Serial.println(Read16BitValue(i));
    }
}

bool CheckIsFirstRun()
{
    if (Read8BitValue(1023) != 100)
    {
        Serial.println("First run, storing default positions");
        Store8BitValue(1023, 100);

        // Set Default Positions (will definitely be wrong)
        StorePickupPos(373);
        StoreScanPos(688);
        StoreDropoffPos(878);

        for (int i = 0; i < 7; i++)
        {
            StoreColour(i, defaultColors[i][0], defaultColors[i][1], defaultColors[i][2]); // Get and set red
        }

        return true;
    }
    else
    {
        return false;
    }
}

void LoadColoursFromEEPROM()
{
    for (int i = 0; i < 7; i++)
    {
        SetColour(i, Read16BitValue(10 + i * 8 + 0), Read16BitValue(10 + i * 8 + 2), Read16BitValue(10 + i * 8 + 4)); // Get and set rgb
    }
}

// Color storage starts at 10
// Each color takes 3 * 2byte values
void StoreColour(uint8_t colorIdx, uint16_t r, uint16_t g, uint16_t b)
{
    Store16BitValue(10 + colorIdx * 8 + 0, r);
    Store16BitValue(10 + colorIdx * 8 + 2, g);
    Store16BitValue(10 + colorIdx * 8 + 4, b);
}

void StorePickupPos(uint16_t pos)
{
    Store16BitValue(0, pos);
}

int LoadPickupPos()
{
    return Read16BitValue(0);
}

void StoreScanPos(uint16_t pos)
{
    Store16BitValue(2, pos);
}

int LoadScanPos()
{
    return Read16BitValue(2);
}

void StoreDropoffPos(uint16_t pos)
{
    Store16BitValue(4, pos);
}

int LoadDropoffPos()
{
    return Read16BitValue(4);
}

void Store8BitValue(uint16_t idx, uint8_t val)
{
    EEPROM.write(idx, val);
}

void Store16BitValue(uint16_t idx, int val)
{
    EEPROM.write(idx, val);
    EEPROM.write(idx + 1, val >> 8);
}

uint8_t Read8BitValue(uint16_t idx)
{
    return EEPROM.read(idx);
}

int Read16BitValue(uint16_t idx)
{
    int val;

    val = (EEPROM.read(idx + 1) << 8);
    val |= EEPROM.read(idx);

    return val;
}

#line 1 "e:\\nextcloud\\Projects\\ColorSorter\\firmware\\colorsorter\\rotation.ino"


uint16_t POS_DROP;
uint16_t POS_SCAN;
uint16_t POS_PICKUP; 

#define EN 4
#define STEP 2
#define DIR 3

#define MS1 10
#define MS2 9
#define MS3 8


bool homing = true;

int pos = 0;

bool isHomed = false;

void InitRotation()
{
    pinMode(EN, OUTPUT);
    pinMode(STEP, OUTPUT);
    pinMode(DIR, OUTPUT);

    pinMode(MS1, OUTPUT);
    pinMode(MS2, OUTPUT);
    pinMode(MS3, OUTPUT);
    digitalWrite(MS1, HIGH);
    digitalWrite(MS2, HIGH);
    digitalWrite(MS3, HIGH);

    EnableStepMotor(true);
    digitalWrite(DIR, CCW);

    CheckIsFirstRun();

    Serial.println("Loading positions from EEPROM");
    POS_DROP = LoadDropoffPos();
    POS_SCAN = LoadScanPos();
    POS_PICKUP = LoadPickupPos();
}

bool IsHomed(){
    return isHomed;
}

void SetIsHomed(bool _isHomed){
    isHomed = _isHomed;
}

void Vibrate()
{
    bool vibrateDir = true;
    digitalWrite(DIR, vibrateDir);
    for (int i = 0; i < 80; i++)
    {
        for (int z = 0; z < 17; z++)
        {
            digitalWrite(STEP, HIGH);
            delayMicroseconds(200);
            digitalWrite(STEP, LOW);
            delayMicroseconds(200);
        }
        vibrateDir = !vibrateDir;
        digitalWrite(DIR, vibrateDir);
    }
}

int shorterDirection(int crt, int next)
{
    int toRight = (next - crt + 1023) % 1023;
    int toLeft = (crt - next + 1023) % 1023;
    if (toLeft < toRight)
    {
        return CW;
    }
    else
    {
        return CCW;
    }
}

bool ReachedDestination(int targetPos)
{
    int diff = Difference(pos, targetPos);
    //Serial.println(diff);
    return diff < 2;
}

void SetDirection(int dir)
{
    digitalWrite(DIR, dir);
}

void SetDirectionCW()
{
    digitalWrite(DIR, CW);
}

void SetDirectionCCW()
{
    digitalWrite(DIR, CCW);
}

void UpdatePos(){
    pos = analogRead(POS_PIN);
}

void Step()
{
    digitalWrite(STEP, HIGH);
    delayMicroseconds(50);
    digitalWrite(STEP, LOW);
    delayMicroseconds(1400 - 50);    
}

void Step(int steps, int dir)
{
    digitalWrite(DIR, dir);

    for (int i = 0; i < abs(steps); i++)
    {
        Step();
    }
}

void EnableStepMotor(bool enabled)
{
    if (enabled)
    {
        Serial.println("Motor Enabled");
    }
    else
    {
        Serial.println("Motor Disabled");
    }
    digitalWrite(EN, !enabled); //LOW enabled, HIGH disabled
}


int Difference(int v0, int v1)
{
  if (v0 > v1)
  {
    return v0 - v1;
  }
  else
  {
    return v1 - v0;
  }
}

int GetPos(){
    return pos;
}

int GetPosPickup(){
    return POS_PICKUP;
}

int GetPosDrop(){
    return POS_DROP;
}

int GetPosScan(){
    return POS_SCAN;
}

void SavePosPickup(){
    StorePickupPos(GetPos());
    POS_PICKUP = GetPos();
    Serial.print("Storing Pos Pickup: ");
    Serial.println(POS_PICKUP);
}

void SavePosDrop(){
    StoreDropoffPos(GetPos());    
    POS_DROP = GetPos();
    Serial.print("Storing Pos Drop: ");
    Serial.println(POS_DROP);
}

void SavePosScan(){
    StoreScanPos(GetPos());    
    POS_SCAN = GetPos();
    Serial.print("Storing Pos Scan: ");
    Serial.println(POS_SCAN);
}
