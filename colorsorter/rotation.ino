

uint16_t POS_DROP;
uint16_t POS_SCAN;
uint16_t POS_PICKUP; 

#define EN 4
#define STEP 2
#define DIR 3

#define MS1 10
#define MS2 9
#define MS3 8


bool homing = true;

int pos = 0;

bool isHomed = false;

void InitRotation()
{
    pinMode(EN, OUTPUT);
    pinMode(STEP, OUTPUT);
    pinMode(DIR, OUTPUT);

    pinMode(MS1, OUTPUT);
    pinMode(MS2, OUTPUT);
    pinMode(MS3, OUTPUT);
    digitalWrite(MS1, HIGH);
    digitalWrite(MS2, HIGH);
    digitalWrite(MS3, HIGH);

    EnableStepMotor(true);
    digitalWrite(DIR, CCW);

    CheckIsFirstRun();

    Serial.println("Loading positions from EEPROM");
    POS_DROP = LoadDropoffPos();
    POS_SCAN = LoadScanPos();
    POS_PICKUP = LoadPickupPos();
}

bool IsHomed(){
    return isHomed;
}

void SetIsHomed(bool _isHomed){
    isHomed = _isHomed;
}

void Vibrate()
{
    bool vibrateDir = true;
    digitalWrite(DIR, vibrateDir);
    for (int i = 0; i < 80; i++)
    {
        for (int z = 0; z < 17; z++)
        {
            digitalWrite(STEP, HIGH);
            delayMicroseconds(200);
            digitalWrite(STEP, LOW);
            delayMicroseconds(200);
        }
        vibrateDir = !vibrateDir;
        digitalWrite(DIR, vibrateDir);
    }
}

int shorterDirection(int crt, int next)
{
    int toRight = (next - crt + 1023) % 1023;
    int toLeft = (crt - next + 1023) % 1023;
    if (toLeft < toRight)
    {
        return CW;
    }
    else
    {
        return CCW;
    }
}

bool ReachedDestination(int targetPos)
{
    int diff = Difference(pos, targetPos);
    //Serial.println(diff);
    return diff < 2;
}

void SetDirection(int dir)
{
    digitalWrite(DIR, dir);
}

void SetDirectionCW()
{
    digitalWrite(DIR, CW);
}

void SetDirectionCCW()
{
    digitalWrite(DIR, CCW);
}

void UpdatePos(){
    pos = analogRead(POS_PIN);
}

void Step()
{
    digitalWrite(STEP, HIGH);
    delayMicroseconds(50);
    digitalWrite(STEP, LOW);
    delayMicroseconds(1400 - 50);    
}

void Step(int steps, int dir)
{
    digitalWrite(DIR, dir);

    for (int i = 0; i < abs(steps); i++)
    {
        Step();
    }
}

void EnableStepMotor(bool enabled)
{
    if (enabled)
    {
        Serial.println("Motor Enabled");
    }
    else
    {
        Serial.println("Motor Disabled");
    }
    digitalWrite(EN, !enabled); //LOW enabled, HIGH disabled
}


int Difference(int v0, int v1)
{
  if (v0 > v1)
  {
    return v0 - v1;
  }
  else
  {
    return v1 - v0;
  }
}

int GetPos(){
    return pos;
}

int GetPosPickup(){
    return POS_PICKUP;
}

int GetPosDrop(){
    return POS_DROP;
}

int GetPosScan(){
    return POS_SCAN;
}

void SavePosPickup(){
    StorePickupPos(GetPos());
    POS_PICKUP = GetPos();
    Serial.print("Storing Pos Pickup: ");
    Serial.println(POS_PICKUP);
}

void SavePosDrop(){
    StoreDropoffPos(GetPos());    
    POS_DROP = GetPos();
    Serial.print("Storing Pos Drop: ");
    Serial.println(POS_DROP);
}

void SavePosScan(){
    StoreScanPos(GetPos());    
    POS_SCAN = GetPos();
    Serial.print("Storing Pos Scan: ");
    Serial.println(POS_SCAN);
}