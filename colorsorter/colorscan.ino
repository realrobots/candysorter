
int averageRed[32];
int averageGreen[32];
int averageBlue[32];
int count = 0;

int color[] = {0, 0, 0};
bool averageReady = false;

int colors[7][3] = {{972, 941, 940},  // YELLOW
                    {955, 933, 937},  // GREEN
                    {973, 927, 936},  // ORANGE
                    {956, 921, 935},  // BROWN
                    {965, 920, 934},  // RED
                    {800, 800, 800},  // OPEN
                    {972, 944, 956}}; // WHITE (CLOSED)

String colorLabels[] = {"YELLOW", "GREEN", "ORANGE", "BROWN", "RED", "OPEN", "CLOSED"};

void LoadColours()
{
  LoadColoursFromEEPROM();
}

void SetColour(int colorIdx, int r, int g, int b)
{
  colors[colorIdx][0] = r;
  colors[colorIdx][1] = g;
  colors[colorIdx][2] = b;
}

void RollingAverage()
{
  if (count < 32)
  {
    averageRed[count] = color[0];
    averageGreen[count] = color[1];
    averageBlue[count] = color[2];
    count++;
  }
  else
  {
    averageReady = true;
    count = 0;
  }
}

void SampleColour()
{

  pixels.setPixelColor(0, pixels.Color(255, 0, 0));
  pixels.show();
  color[0] = GetAverage();

  pixels.setPixelColor(0, pixels.Color(0, 255, 0));
  pixels.show();
  delay(5);
  color[1] = GetAverage();

  pixels.setPixelColor(0, pixels.Color(0, 0, 255));
  pixels.show();
  color[2] = GetAverage();

  pixels.setPixelColor(0, pixels.Color(0, 0, 0));
  pixels.show();

  Serial.print("R: ");
  Serial.print(color[0]);
  Serial.print("\tG: ");
  Serial.print(color[1]);
  Serial.print("\tB: ");
  Serial.print(color[2]);
  Serial.println();
}

void StoreCurrentColour(int colorIdx)
{
  Serial.print("Storing ");
  Serial.print(colorLabels[colorIdx]);
  Serial.print(": ");
  Serial.print(color[0]);
  Serial.print("\t");
  Serial.print(color[1]);
  Serial.print("\t");
  Serial.print(color[2]);
  Serial.print("\n");
  StoreColour(colorIdx, color[0], color[1], color[2]);
  SetColour(colorIdx, color[0], color[1], color[2]);
}

void TryMatchColours()
{
  int match[] = {0, 0, 0, 0, 0};
  int r;
  int g;
  int b;
  int closest = 0;
  // Serial.print('R: ');
  // Serial.print(color[0]);
  // Serial.print('\tG: ');
  // Serial.print(color[1]);
  // Serial.print('\tB: ');
  // Serial.print(color[2]);
  // Serial.println('\n');

  //Serial.print("Match\t");
  for (int i = 0; i < 5; i++)
  {
    r = Difference(color[0], colors[i][0]);
    g = Difference(color[1], colors[i][1]);
    b = Difference(color[2], colors[i][2]);
    match[i] = r + g + b;

    // Serial.print(colorLabels[i]);
    // Serial.print(":");
    // Serial.print(match[i]);
    // Serial.print("\t");
    if (i != 0)
    {
      if (match[i] < match[closest])
      {
        closest = i;
      }
    }
  }
  Serial.println();
  Serial.print("I guess the colour is ");
  Serial.println(colorLabels[closest]);
  currentColorIdx = closest;

  Serial.print(colors[closest][0]);
  Serial.print("\t");
  Serial.print(colors[closest][1]);
  Serial.print("\t");
  Serial.print(colors[closest][2]);
  Serial.print("\n");
}

int GetAverage()
{
  total = 0;
  delay(3);
  for (int i = 0; i < 32; i++)
  {
    total += analogRead(27);
    delay(1);
  }
  return total / 32;
}

int GetRollingAverage(int colorIdx)
{
  if (!averageReady)
  {
    return -100;
  }
  else
  {
    int total = 0;
    for (int i = 0; i < 32; i++)
    {
      if (colorIdx == 0)
      {
        total += averageRed[i];
      }
      else if (colorIdx == 1)
      {
        total += averageGreen[i];
      }
      else
      {
        total += averageBlue[i];
      }
    }
    return total / 32;
  }
}

int DistanceFromAverage()
{
  int colorNow = (color[0] + color[1] + color[2]) / 3;
  int average = (GetRollingAverage(0) + GetRollingAverage(1) + GetRollingAverage(2)) / 3;

  return abs(colorNow - average);
}
