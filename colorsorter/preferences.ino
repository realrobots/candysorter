#include <EEPROM.h>

int defaultColors[7][3] = {{972, 941, 940},  // YELLOW
                        {955, 933, 937},  // GREEN
                        {973, 927, 936},  // ORANGE
                        {956, 921, 935},  // BROWN
                        {965, 920, 934},  // RED
                        {800, 800, 800},   // OPEN
                        {972, 944, 956}}; // WHITE (CLOSED)

void EEPROMReport(){
    
    Serial.print("PICKUP POS: ");
    Serial.println(LoadPickupPos());
    Serial.print("DROP POS: ");
    Serial.println(LoadDropoffPos());
    Serial.print("SCAN POS: ");
    Serial.println(LoadScanPos());


    Serial.println("COLORS");
    for (int i = 10; i < 10+8*8 + 4; i+= 2){
        Serial.print(i);
        Serial.print(": ");
        Serial.println(Read16BitValue(i));
    }
}

bool CheckIsFirstRun()
{
    if (Read8BitValue(1023) != 100)
    {
        Serial.println("First run, storing default positions");
        Store8BitValue(1023, 100);

        // Set Default Positions (will definitely be wrong)
        StorePickupPos(373);
        StoreScanPos(688);
        StoreDropoffPos(878);

        for (int i = 0; i < 7; i++)
        {
            StoreColour(i, defaultColors[i][0], defaultColors[i][1], defaultColors[i][2]); // Get and set red
        }

        return true;
    }
    else
    {
        return false;
    }
}

void LoadColoursFromEEPROM()
{
    for (int i = 0; i < 7; i++)
    {
        SetColour(i, Read16BitValue(10 + i * 8 + 0), Read16BitValue(10 + i * 8 + 2), Read16BitValue(10 + i * 8 + 4)); // Get and set rgb
    }
}

// Color storage starts at 10
// Each color takes 3 * 2byte values
void StoreColour(uint8_t colorIdx, uint16_t r, uint16_t g, uint16_t b)
{
    Store16BitValue(10 + colorIdx * 8 + 0, r);
    Store16BitValue(10 + colorIdx * 8 + 2, g);
    Store16BitValue(10 + colorIdx * 8 + 4, b);
}

void StorePickupPos(uint16_t pos)
{
    Store16BitValue(0, pos);
}

int LoadPickupPos()
{
    return Read16BitValue(0);
}

void StoreScanPos(uint16_t pos)
{
    Store16BitValue(2, pos);
}

int LoadScanPos()
{
    return Read16BitValue(2);
}

void StoreDropoffPos(uint16_t pos)
{
    Store16BitValue(4, pos);
}

int LoadDropoffPos()
{
    return Read16BitValue(4);
}

void Store8BitValue(uint16_t idx, uint8_t val)
{
    EEPROM.write(idx, val);
}

void Store16BitValue(uint16_t idx, int val)
{
    EEPROM.write(idx, val);
    EEPROM.write(idx + 1, val >> 8);
}

uint8_t Read8BitValue(uint16_t idx)
{
    return EEPROM.read(idx);
}

int Read16BitValue(uint16_t idx)
{
    int val;

    val = (EEPROM.read(idx + 1) << 8);
    val |= EEPROM.read(idx);

    return val;
}
